##Java

- Please note, the Intellij formatter will pick up on many of the structural problems, but checkstyle looks at far more than that.  So running the automatic formatter on your code does not mean that it will automatically pass the checkstyle rules and you will still need to manually prepare certain things.

- If there is a specific formatting you are trying to achieve with regards to where newlines are positioned in the line, then use it and the reformatter shouldn't change it.

###Intellij Formatting

- You can copy this settings file to your Intellij IDE project settings to get the formatting and code inspections required for this style guide.

- First, you need to go to File -> Settings -> Editor -> Code Style -> Java, click Manage... then select the Default Profile and then click Copy to Project. Then select the Project profile.

- This creates a file in the project's .idea called "codeStyleSettings.xml" and also tells intellij to use it.

- Next we need to copy Skipjaq's own codeStyleSettings.xml into that directory.  In this repository, copy the codeStyleSettings.xml file from the 'java' directory and place it in the .idea directory for the required project, overwriting the existing one.

###Checkstyle

- You can make use of the checkstyle xml itself for more thorough analysis of the code by importing the rules file.

- You will need to install the IDEA Checkstyle plugin.  Go to File -> Settings -> Plugins -> Browse Repositories then search for Checkstyle. Install CheckStyle IDEA.

- Once installed, to use the rules file go to File -> Settings -> Other Settings -> Checkstyle then in the Configuration File area click the '+' to import the file.  Then check the box on the left to make it the active configuration.

- You can use the plugin by right clicking a file and selecting the "Check Current File".

###Known problems
- There is currently a known problem with the Intellij formatter when there are static imports of multiple groups.  The style guide demands that these are separated but this does not seem to be currently possible in an Intellij formatter.
- Additionally there is a known problem with the arrangement of operators on a new line, the checkstyle expects them to be on the newline, intellij cannot put them there currently.

###Supress Findbugs
- If you have a problem with a Findbugs rule in a specific situation, and you are convinced that the rule is not appropriate in that scenario, then feel free to supress it. E.g.

`@edu.umd.cs.findbugs.annotations.SuppressWarnings({"MS_SHOULD_BE_FINAL" , "URF_UNREAD_PUBLIC_OR_PROTECTED_FIELD"})`
